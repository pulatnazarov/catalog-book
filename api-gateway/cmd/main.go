package main

import (
	_ "api-gateway/docs"
	"api-gateway/handler"
	"api-gateway/service"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title           Book Crud API
// @version         1.0
// @description     This is a sample server celler server.

// NewRouter
// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io
func main() {
	h := handler.New(service.New("localhost:9001"))

	r := gin.Default()

	r.POST("/book", h.RegisterBook)
	r.GET("/book/:id", h.GetBook)
	r.PUT("/book/update", h.UpdateBook)
	r.DELETE("/book/delete/:id", h.DeleteBook)
	r.GET("/books", h.GetBooks)

	r.POST("/author/:name", h.CreateAuthor)

	r.POST("/category/:name", h.CreateCategory)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":9090")
}
