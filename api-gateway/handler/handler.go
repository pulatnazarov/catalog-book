package handler

import (
	"api-gateway/entity"
	"api-gateway/service"
	"context"
	"github.com/gin-gonic/gin"
)

type Handler struct {
	service service.Service
}

func New(service service.Service) Handler {
	return Handler{
		service: service,
	}
}

// GetBooks
// @Summary      Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Success      200 {object} []entity.Book
// @Failure      400
// @Failure      500
// @Router       /books [GET]
func (h Handler) GetBooks(c *gin.Context) {
	books, err := h.service.BookService.GetBooks(context.Background())
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, books)
}

// DeleteBook
// @Summary      Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id path string true "DeleteBook"
// @Success      200
// @Failure      400
// @Failure      500
// @Router       /book/delete/{id} [DELETE]
func (h Handler) DeleteBook(c *gin.Context) {
	id := c.Param("id")

	if err := h.service.BookService.DeleteBook(context.Background(), id); err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, gin.H{
		"ok": "deleted",
	})
}

// UpdateBook
// @Summary      Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body entity.UpdateBookRequest true "UpdateBook"
// @Success      200 {object} entity.Book
// @Failure      400
// @Failure      500
// @Router       /book/update [PUT]
func (h Handler) UpdateBook(c *gin.Context) {
	var request entity.UpdateBookRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	book, err := h.service.BookService.UpdateBook(context.Background(), request)
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, book)
}

// GetBook
// @Summary      Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id path string true "GetBook"
// @Success      200 {object} entity.Book
// @Failure      400
// @Failure      500
// @Router       /book/{id} [GET]
func (h Handler) GetBook(c *gin.Context) {
	id := c.Param("id")
	b, err := h.service.BookService.GetBook(context.Background(), id)
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, b)
}

// RegisterBook
// @Summary      Book
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body entity.RegisterBookRequest true "RegisterBook"
// @Success      200 {object} entity.Book
// @Failure      400
// @Failure      500
// @Router       /book [POST]
func (h Handler) RegisterBook(c *gin.Context) {
	var request entity.RegisterBookRequest
	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}

	book, err := h.service.BookService.RegisterBook(context.Background(), request)
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, book)
}

// CreateAuthor
// @Summary      Author
// @Description
// @Tags         author
// @Accept       json
// @Produce      json
// @Param        name path string true "CreateAuthor"
// @Success      200 {object} entity.Author
// @Failure      400
// @Failure      500
// @Router       /author/{name} [POST]
func (h Handler) CreateAuthor(c *gin.Context) {
	name := c.Param("name")

	author, err := h.service.BookService.CreateAuthor(context.Background(), name)
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, author)
}

// CreateCategory
// @Summary      Category
// @Description
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        name path string true "CreateCategory"
// @Success      200 {object} entity.Category
// @Failure      400
// @Failure      500
// @Router       /category/{name} [POST]
func (h Handler) CreateCategory(c *gin.Context) {
	name := c.Param("name")

	category, err := h.service.BookService.CreateCategory(context.Background(), name)
	if err != nil {
		c.JSON(500, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, category)
}
