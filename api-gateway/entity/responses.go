package entity

type Book struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	PublishedYear int32  `json:"published_year"`
	AuthorId      string `json:"author_id"`
	CategoryId    string `json:"category_id"`
}

type Author struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type Category struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
