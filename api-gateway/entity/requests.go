package entity

type RegisterBookRequest struct {
	Name          string `json:"name"`
	PublishedYear int32  `json:"published_year"`
	AuthorID      string `json:"author_id"`
	CategoryID    string `json:"category_id"`
}

type UpdateBookRequest struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	PublishedYear int32  `json:"published_year"`
	AuthorID      string `json:"author_id"`
	CategoryID    string `json:"category_id"`
}
