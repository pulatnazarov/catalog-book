package repository

import "github.com/google/uuid"

type Book struct {
	ID            uuid.UUID `db:"id"`
	Name          string    `db:"name"`
	PublishedYear int       `db:"published_year"`
	AuthorID      uuid.UUID `db:"author_id"`
	CategoryID    uuid.UUID `db:"category_id"`
}
