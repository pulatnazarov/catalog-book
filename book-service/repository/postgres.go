package repository

import (
	"book-service/config"
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"book-service/pkg/errs"
	"context"
	"database/sql"
	"errors"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type Postgres struct {
	db *sqlx.DB
}

func NewPostgres(cfg config.PostgresConfig) (*Postgres, error) {
	db, err := connect(cfg)
	if err != nil {
		return nil, err
	}

	return &Postgres{
		db: db,
	}, nil
}

func (p *Postgres) GetBooks(ctx context.Context) ([]book.Book, error) {
	return p.getBooks(ctx)
}

func (p *Postgres) getBooks(ctx context.Context) ([]book.Book, error) {
	query := `select * from books`
	books := make([]Book, 0)

	if err := p.db.SelectContext(ctx, &books, query); err != nil {
		return nil, err
	}

	return convertFunc(books), nil
}

func (p *Postgres) DeleteBook(ctx context.Context, id uuid.UUID) error {
	return p.deleteBook(ctx, id)
}

func (p *Postgres) deleteBook(ctx context.Context, id uuid.UUID) error {
	query := `delete from books where id = $1`

	_, err := p.db.ExecContext(ctx, query, id)

	return err
}

func (p *Postgres) UpdateBook(ctx context.Context, b book.Book) (book.Book, error) {
	newB, err := p.updateBook(ctx, b)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return book.Book{}, errs.ErrNotFound
		}
		return book.Book{}, err
	}

	return book.UnmarshalBook(book.UnmarshalBookArgs(newB))
}

func (p *Postgres) updateBook(ctx context.Context, b book.Book) (Book, error) {
	query := `update books set name = $1, published_year = $2, author_id = $3, category_id = $4 where id = $5`
	if _, err := p.db.ExecContext(ctx, query, b.Name(), b.PublishedYear(), b.AuthorID(), b.CategoryID(), b.ID()); err != nil {
		return Book{}, err
	}

	var newB Book
	query = "select * from books where id = $1"
	if err := p.db.GetContext(ctx, &newB, query, b.ID()); err != nil {
		return Book{}, err
	}

	return newB, nil
}

func (p *Postgres) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := p.getBook(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return book.Book{}, errs.ErrNotFound
		}
		return book.Book{}, err
	}

	return book.UnmarshalBook(book.UnmarshalBookArgs(b))
}

func (p *Postgres) getBook(ctx context.Context, id uuid.UUID) (Book, error) {
	query := `select * from books where id = $1`

	var b Book
	if err := p.db.GetContext(ctx, &b, query, id); err != nil {
		return Book{}, err
	}

	return b, nil
}

func (p *Postgres) CreateAuthor(ctx context.Context, a author.Author) error {
	return p.createAuthor(ctx, a)
}

func (p *Postgres) createAuthor(ctx context.Context, a author.Author) error {
	query := `insert into authors values ($1, $2)`

	_, err := p.db.ExecContext(ctx, query, a.ID(), a.Name())
	return err
}

func (p *Postgres) CreateCategory(ctx context.Context, c category.Category) error {
	return p.createCategory(ctx, c)
}

func (p *Postgres) createCategory(ctx context.Context, c category.Category) error {
	query := `insert into categories values ($1, $2)`

	_, err := p.db.ExecContext(ctx, query, c.ID(), c.Name())
	return err
}

func (p *Postgres) CreateBook(ctx context.Context, b book.Book) error {
	return p.createBook(ctx, b)
}

func (p *Postgres) createBook(ctx context.Context, b book.Book) error {
	query := `insert into books values ($1, $2, $3, $4, $5)`

	_, err := p.db.ExecContext(ctx, query, b.ID(), b.Name(), b.PublishedYear(), b.AuthorID(), b.CategoryID())
	return err
}

func convertFunc(books []Book) []book.Book {
	newBooks := make([]book.Book, 0)
	for _, b := range books {
		newBook, err := book.UnmarshalBook(book.UnmarshalBookArgs(b))
		if err != nil {
			return nil
		}
		newBooks = append(newBooks, newBook)
	}
	return newBooks
}

func (p *Postgres) Cleanup(ctx context.Context) error {
	query := `delete from books`
	_, err := p.db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `delete from authors`
	_, err = p.db.ExecContext(ctx, query)
	if err != nil {
		return err
	}

	query = `delete from categories`
	_, err = p.db.ExecContext(ctx, query)
	return err
}
