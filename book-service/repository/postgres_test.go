package repository

import (
	"book-service/config"
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"book-service/pkg/id"
	"context"
	"github.com/stretchr/testify/require"
	"log"
	"testing"
)

var testPostgresCfg = config.PostgresConfig{
	PostgresHost:           "localhost",
	PostgresPort:           "5432",
	PostgresUser:           "pulat",
	PostgresPassword:       "9",
	PostgresDB:             "book_db",
	PostgresMigrationsPath: "/home/pulat/out of go/Projects/catolog_book/book-service/repository/migrations",
}

func TestPostgres_Book(t *testing.T) {
	p, err := NewPostgres(testPostgresCfg)
	require.NoError(t, err)

	t.Cleanup(cleanup(p))

	authorFactory := author.NewFactory(id.Generator{})
	categoryFactory := category.NewFactory(id.Generator{})
	bookFactory := book.NewFactory(id.Generator{})

	t.Run("create author", func(t *testing.T) {
		t.Cleanup(cleanup(p))
		a, err := authorFactory.NewAuthor("William Shakespeare")

		err = p.CreateAuthor(context.Background(), a)
		require.NoError(t, err)
	})

	t.Run("create category", func(t *testing.T) {
		t.Cleanup(cleanup(p))
		c, err := categoryFactory.NewCategory("Romantic")

		err = p.CreateCategory(context.Background(), c)
		require.NoError(t, err)
	})

	t.Run("create book", func(t *testing.T) {
		t.Cleanup(cleanup(p))
		a, err := authorFactory.NewAuthor("William Shakespeare")

		err = p.CreateAuthor(context.Background(), a)
		require.NoError(t, err)

		c, err := categoryFactory.NewCategory("Romantic")

		err = p.CreateCategory(context.Background(), c)
		require.NoError(t, err)

		b, err := bookFactory.NewBook("Romeo and Juliet", 1759, a.ID(), c.ID())

		err = p.CreateBook(context.Background(), b)
		require.NoError(t, err)
	})
}

func cleanup(p *Postgres) func() {
	return func() {
		if err := p.Cleanup(context.Background()); err != nil {
			log.Panicln("failed to cleanup db, should be done manually", err)
		}
	}
}
