create table categories (
    id uuid primary key,
    name varchar(50) not null
);

create table authors (
    id uuid primary key,
    name varchar(50) not null
);

create table books (
    id uuid primary key,
    name varchar(50) not null unique,
    published_year int not null,
    author_id uuid not null references authors(id),
    category_id uuid not null references categories(id)
);
