package main

import (
	"book-service/bookpb"
	"book-service/config"
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"book-service/pkg/id"
	"book-service/repository"
	"book-service/server"
	"book-service/service"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		log.Println("error with loading config")
		return
	}

	repo, err := repository.NewPostgres(cfg.PostgresConfig)
	if err != nil {
		log.Println("error while creating Postgres struct")
		return
	}

	authorFactory := author.NewFactory(id.Generator{})
	categoryFactory := category.NewFactory(id.Generator{})
	bookFactory := book.NewFactory(id.Generator{})

	svc := service.New(repo, authorFactory, categoryFactory, bookFactory)
	server := server.New(svc, authorFactory, categoryFactory, bookFactory)

	lis, err := net.Listen("tcp", net.JoinHostPort(cfg.Host, cfg.Port))
	if err != nil {
		log.Println("error with joinHostPort")
	}

	grpcServer := grpc.NewServer()

	bookpb.RegisterBookServiceServer(grpcServer, server)

	if err = grpcServer.Serve(lis); err != nil {
		log.Println("error while serving grpc")
	}
}
