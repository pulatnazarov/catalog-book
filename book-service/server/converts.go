package server

import (
	"book-service/bookpb"
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
)

func toProtoBooks(books []book.Book) *bookpb.Books {
	var protoBooks []*bookpb.Book
	for _, b := range books {
		protoBooks = append(protoBooks, toProtoBook(b))
	}
	return &bookpb.Books{Books: protoBooks}
}

func toProtoBook(b book.Book) *bookpb.Book {
	return &bookpb.Book{
		Id:            b.ID().String(),
		Name:          b.Name(),
		PublishedYear: int32(b.PublishedYear()),
		AuthorId:      b.AuthorID().String(),
		CategoryId:    b.CategoryID().String(),
	}
}

func toProtoAuthor(a author.Author) *bookpb.Author {
	return &bookpb.Author{
		Id:   a.ID().String(),
		Name: a.Name(),
	}
}

func toProtoCategory(c category.Category) *bookpb.Category {
	return &bookpb.Category{
		Id:   c.ID().String(),
		Name: c.Name(),
	}
}
