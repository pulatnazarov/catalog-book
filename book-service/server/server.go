package server

import (
	"book-service/bookpb"
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"book-service/service"
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	bookpb.UnimplementedBookServiceServer
	service         service.Service
	authorFactory   author.Factory
	categoryFactory category.Factory
	bookFactory     book.Factory
}

func New(svc service.Service, authorFactory author.Factory, categoryFactory category.Factory, bookFactory book.Factory) Server {
	return Server{
		service:         svc,
		authorFactory:   authorFactory,
		categoryFactory: categoryFactory,
		bookFactory:     bookFactory,
	}
}

func (s Server) RegisterBook(ctx context.Context, req *bookpb.RegisterBookRequest) (*bookpb.Book, error) {
	book, err := s.converRegisterBookRequestToDomainBook(req)
	if err != nil {
		return nil, err
	}

	createdBook, err := s.service.RegisterBook(ctx, book)
	if err != nil {
		return nil, err
	}

	return toProtoBook(createdBook), nil
}

func (s Server) CreateAuthor(ctx context.Context, req *bookpb.CreateAuthorRequest) (*bookpb.Author, error) {
	author, err := s.authorFactory.NewAuthor(req.Name)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	createdAuthor, err := s.service.CreateAuthor(ctx, author)
	if err != nil {
		return nil, status.Error(codes.Canceled, err.Error())
	}
	return toProtoAuthor(createdAuthor), nil
}

func (s Server) CreateCategory(ctx context.Context, req *bookpb.CreateCategoryRequest) (*bookpb.Category, error) {
	category, err := s.categoryFactory.NewCategory(req.Name)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	createdCategory, err := s.service.CreateCategory(ctx, category)
	if err != nil {
		return nil, status.Error(codes.Canceled, err.Error())
	}

	return toProtoCategory(createdCategory), nil
}

func (s Server) GetBook(ctx context.Context, req *bookpb.GetBookRequest) (*bookpb.Book, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "id is not uuid")
	}

	b, err := s.service.GetBook(ctx, id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return toProtoBook(b), nil
}

func (s Server) UpdateBook(ctx context.Context, req *bookpb.UpdateBookRequest) (*bookpb.Book, error) {
	book, err := s.convertUpdateBookRequestToDomainBook(req)
	if err != nil {
		return nil, err
	}

	updatedBook, err := s.service.UpdateBook(ctx, book)
	if err != nil {
		return nil, err
	}

	return toProtoBook(updatedBook), nil
}

func (s Server) DeleteBook(ctx context.Context, id *bookpb.DeleteBookRequest) (*emptypb.Empty, error) {
	bookId, err := uuid.Parse(id.GetId())
	if err != nil {
		return &emptypb.Empty{}, err
	}
	err = s.service.DeleteBook(ctx, bookId)

	return &emptypb.Empty{}, err
}

func (s Server) GetBooks(ctx context.Context, empty *emptypb.Empty) (*bookpb.Books, error) {
	books, err := s.service.GetBooks(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return toProtoBooks(books), nil
}

func (s Server) convertUpdateBookRequestToDomainBook(protoBook *bookpb.UpdateBookRequest) (book.Book, error) {
	authorID, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided author id is not uuid")
	}
	categoryID, err := uuid.Parse(protoBook.CategoryId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided category id is not uuid")
	}

	bookId, err := uuid.Parse(protoBook.GetId())
	if err != nil {
		return book.Book{}, err
	}

	unmarshaledBook := book.UnmarshalBookArgs{
		ID:            bookId,
		Name:          protoBook.Name,
		PublishedYear: int(protoBook.PublishedYear),
		AuthorID:      authorID,
		CategoryID:    categoryID,
	}

	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return book.UnmarshalBook(unmarshaledBook)
}

func (s Server) converRegisterBookRequestToDomainBook(protoBook *bookpb.RegisterBookRequest) (book.Book, error) {
	authorID, err := uuid.Parse(protoBook.AuthorId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided author id is not uuid")
	}
	categoryID, err := uuid.Parse(protoBook.CategoryId)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, "provided category id is not uuid")
	}

	b, err := s.bookFactory.NewBook(
		protoBook.Name,
		int(protoBook.PublishedYear),
		authorID,
		categoryID,
	)
	if err != nil {
		return book.Book{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return b, nil
}
