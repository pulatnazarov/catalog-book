package category

import (
	"book-service/pkg/id"
)

type Factory struct {
	idGenerator id.IGenerator
}

func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

func (f Factory) NewCategory(name string) (Category, error) {
	c := Category{
		id:   f.idGenerator.GenerateUUID(),
		name: name,
	}

	if err := c.validate(); err != nil {
		return Category{}, err
	}

	return c, nil
}
