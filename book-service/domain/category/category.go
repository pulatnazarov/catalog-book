package category

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var ErrInvalidCategoryData = errors.New("invalid category data")

type Category struct {
	id   uuid.UUID
	name string
}

func (c Category) ID() uuid.UUID {
	return c.id
}

func (c Category) Name() string {
	return c.name
}

func (c Category) validate() error {
	if c.name == "" {
		return fmt.Errorf("%w: name is empty", ErrInvalidCategoryData)
	}
	return nil
}
