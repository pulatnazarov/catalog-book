package category

import (
	"github.com/google/uuid"
	"reflect"
	"testing"
)

func TestFactory_NewCategory(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    Category
		wantErr bool
	}{
		{
			name: "should pass",
			args: args{
				name: "Detective",
			},
			want: Category{
				id:   testCategoryID,
				name: "Detective",
			},
			wantErr: false,
		},
		{
			name: "with empty name",
			args: args{
				name: "",
			},
			want:    Category{},
			wantErr: true,
		},
	}

	f := NewFactory(testIDGenerator{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewCategory(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewCategory() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCategory() got = %v, want %v", got, tt.want)
			}
		})
	}
}

type testIDGenerator struct{}

func (g testIDGenerator) GenerateUUID() uuid.UUID {
	return testCategoryID
}

var testCategoryID = uuid.New()
