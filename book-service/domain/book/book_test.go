package book

import (
	"github.com/google/uuid"
	"reflect"
	"testing"
)

func TestBook_NewBook(t *testing.T) {
	type args struct {
		name          string
		publishedYear int
		authorId      uuid.UUID
		categoryId    uuid.UUID
	}

	tests := []struct {
		name    string
		args    args
		want    Book
		wantErr bool
	}{
		{
			name: "should pass",
			args: args{
				name:          "Sherlock Holmes",
				publishedYear: 1887,
				authorId:      testAuthorID,
				categoryId:    testCategoryID,
			},
			want: Book{
				id:            testBookID,
				name:          "Sherlock Holmes",
				publishedYear: 1887,
				authorId:      testAuthorID,
				categoryId:    testCategoryID,
			},
			wantErr: false,
		},
		{
			name: "with empty name",
			args: args{
				name:          "",
				publishedYear: 1887,
				authorId:      testAuthorID,
				categoryId:    testCategoryID,
			},
			want:    Book{},
			wantErr: true,
		},
		{
			name: "with bad published year",
			args: args{
				name:          "Sherlock Holmes",
				publishedYear: 0,
				authorId:      testAuthorID,
				categoryId:    testCategoryID,
			},
			want:    Book{},
			wantErr: true,
		},
	}
	f := NewFactory(testIDGenerator{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewBook(tt.args.name, tt.args.publishedYear, tt.args.authorId, tt.args.categoryId)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewBook() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBook() got = %v, want %v", got, tt.want)
			}
		})
	}
}

var (
	testBookID     = uuid.New()
	testAuthorID   = uuid.New()
	testCategoryID = uuid.New()
)

type testIDGenerator struct{}

func (t testIDGenerator) GenerateUUID() uuid.UUID {
	return testBookID
}
