package book

import (
	"book-service/pkg/id"
	"github.com/google/uuid"
)

type Factory struct {
	idGenerator id.IGenerator
}

func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

func (f Factory) NewBook(name string, publishedYear int, authorId, categoryId uuid.UUID) (Book, error) {
	b := Book{
		id:            f.idGenerator.GenerateUUID(),
		name:          name,
		publishedYear: publishedYear,
		authorId:      authorId,
		categoryId:    categoryId,
	}

	if err := b.validate(); err != nil {
		return Book{}, err
	}

	return b, nil
}
