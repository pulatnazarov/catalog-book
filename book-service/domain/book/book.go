package book

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var ErrInvalidBookData = errors.New("invalid book data")

type Book struct {
	id            uuid.UUID
	name          string
	publishedYear int
	authorId      uuid.UUID
	categoryId    uuid.UUID
}

func (b Book) ID() uuid.UUID {
	return b.id
}

func (b Book) Name() string {
	return b.name
}

func (b Book) PublishedYear() int {
	return b.publishedYear
}

func (b Book) AuthorID() uuid.UUID {
	return b.authorId
}

func (b Book) CategoryID() uuid.UUID {
	return b.categoryId
}

func (b Book) validate() error {
	if b.name == "" {
		return fmt.Errorf("%w: empty book name", ErrInvalidBookData)
	}
	if b.publishedYear <= 0 {
		return fmt.Errorf("%w: negative book's published year", ErrInvalidBookData)
	}
	return nil
}

type UnmarshalBookArgs struct {
	ID            uuid.UUID
	Name          string
	PublishedYear int
	AuthorID      uuid.UUID
	CategoryID    uuid.UUID
}

func UnmarshalBook(args UnmarshalBookArgs) (Book, error) {
	b := Book{
		id:            args.ID,
		name:          args.Name,
		publishedYear: args.PublishedYear,
		authorId:      args.AuthorID,
		categoryId:    args.CategoryID,
	}
	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}
