package author

import (
	"errors"
	"fmt"
	"github.com/google/uuid"
)

var ErrInvalidCategoryData = errors.New("invalid author data")

type Author struct {
	id   uuid.UUID
	name string
}

func (a Author) ID() uuid.UUID {
	return a.id
}

func (a Author) Name() string {
	return a.name
}

func (a Author) validate() error {
	if a.name == "" {
		return fmt.Errorf("%w: name is empty", ErrInvalidCategoryData)
	}

	return nil
}
