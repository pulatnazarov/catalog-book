package author

import (
	"github.com/google/uuid"
	"reflect"
	"testing"
)

func TestFactory_NewAuthor(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    Author
		wantErr bool
	}{
		{
			name: "should pass",
			args: args{
				name: "Arthur Conan Doyle",
			},
			want: Author{
				id:   testAuthorID,
				name: "Arthur Conan Doyle",
			},
			wantErr: false,
		},
		{
			name: "with empty name",
			args: args{
				name: "",
			},
			want:    Author{},
			wantErr: true,
		},
	}

	f := NewFactory(testIDGenerator{})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := f.NewAuthor(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewAuthor() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAuthor() got = %v, want %v", got, tt.want)
			}
		})
	}
}

type testIDGenerator struct{}

func (t testIDGenerator) GenerateUUID() uuid.UUID {
	return testAuthorID
}

var testAuthorID = uuid.New()
