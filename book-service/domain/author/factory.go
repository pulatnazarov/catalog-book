package author

import "book-service/pkg/id"

type Factory struct {
	idGenerator id.IGenerator
}

func NewFactory(idGenerator id.IGenerator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

func (f Factory) NewAuthor(name string) (Author, error) {
	a := Author{
		id:   f.idGenerator.GenerateUUID(),
		name: name,
	}

	if err := a.validate(); err != nil {
		return Author{}, err
	}

	return a, nil
}
