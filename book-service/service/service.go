package service

import (
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"context"
	"github.com/google/uuid"
)

type Service struct {
	repo            Repository
	authorFactory   author.Factory
	categoryFactory category.Factory
	bookFactory     book.Factory
}

func New(repo Repository, authorFactory author.Factory, categoryFactory category.Factory, bookFactory book.Factory) Service {
	return Service{
		repo:            repo,
		authorFactory:   authorFactory,
		categoryFactory: categoryFactory,
		bookFactory:     bookFactory,
	}
}

func (s Service) RegisterBook(ctx context.Context, b book.Book) (book.Book, error) {
	if err := s.repo.CreateBook(ctx, b); err != nil {
		return book.Book{}, err
	}

	return b, nil
}

func (s Service) CreateAuthor(ctx context.Context, a author.Author) (author.Author, error) {
	if err := s.repo.CreateAuthor(ctx, a); err != nil {
		return author.Author{}, err
	}

	return a, nil
}

func (s Service) CreateCategory(ctx context.Context, c category.Category) (category.Category, error) {
	if err := s.repo.CreateCategory(ctx, c); err != nil {
		return category.Category{}, err
	}

	return c, nil
}

func (s Service) GetBook(ctx context.Context, id uuid.UUID) (book.Book, error) {
	b, err := s.repo.GetBook(ctx, id)
	if err != nil {
		return book.Book{}, err
	}

	return b, nil
}

func (s Service) UpdateBook(ctx context.Context, b book.Book) (book.Book, error) {
	b, err := s.repo.UpdateBook(ctx, b)
	if err != nil {
		return book.Book{}, err
	}

	return b, nil
}

func (s Service) DeleteBook(ctx context.Context, id uuid.UUID) error {
	err := s.repo.DeleteBook(ctx, id)
	return err
}

func (s Service) GetBooks(ctx context.Context) ([]book.Book, error) {
	books, err := s.repo.GetBooks(ctx)
	if err != nil {
		return nil, err
	}
	return books, nil
}
