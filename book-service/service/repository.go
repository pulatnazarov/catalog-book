package service

import (
	"book-service/domain/author"
	"book-service/domain/book"
	"book-service/domain/category"
	"context"
	"github.com/google/uuid"
)

type Repository interface {
	BookRepository
	AuthorRepository
	CategoryRepository
}

type BookRepository interface {
	CreateBook(ctx context.Context, b book.Book) error
	GetBook(ctx context.Context, id uuid.UUID) (book.Book, error)
	UpdateBook(ctx context.Context, b book.Book) (book.Book, error)
	DeleteBook(ctx context.Context, id uuid.UUID) error
	GetBooks(ctx context.Context) ([]book.Book, error)
}

type AuthorRepository interface {
	CreateAuthor(ctx context.Context, a author.Author) error
}

type CategoryRepository interface {
	CreateCategory(ctx context.Context, c category.Category) error
}
